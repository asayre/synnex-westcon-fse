# GitLab

*By the end of this lab, you will:*
1. Setup SSH keys and use them to configure gitlab
1. Create and clone a new gitlab repo
1. Commit and push to your new gitlab repo
1. Clone an existing gitlab repo
1. Create a new git repo

---

### Set Up a WorkDir

- Create a directory to house your work for today's class

  ```bash
  mkdir -p ~/sfs
  cd ~/sfs
  ```

---

### Get Started (Authentication)

- Set up [SSH keys](https://docs.gitlab.com/ce/gitlab-basics/create-your-ssh-keys.html)
  - Check to see if you have a public/private key pair in `~/.ssh/`
    - If so, you should see `id_rsa` and `id_rsa.pub` in that directory
    - If not, run `ssh-keygen` to generate a key pair
  - Log in to [GitLab](https://www.gitlab.com), navigate to the `SSH Keys` tab of your profile settings, and add the contents of your `id_rsa.pub` file as a new key.
- Add a [personal access token](https://gitlab.com/profile/personal_access_tokens) with API Scope to your GitLab account settings.
- Use the GitLab API to create a new repository

  ```bash
  TOKEN="<your_personal_access_token>"
  curl -X POST -H "PRIVATE-TOKEN: $TOKEN" "https://gitlab.com/api/v4/projects?name=MyWebserver" | tee results
  ```

  **Note**: [jq is an extremely handy tool](https://stedolan.github.io/jq/) for parsing JSON.  Piping that `curl` output to `tee` displays the results in the terminal as well as storing them in the `results` file.  With `jq`, the results are much more readable:

  ```bash
  cat results | jq
  # or
  cat results | jq '.ssh_url_to_repo'
  ```

---

#### Why API?

Using Application Programming Interfaces (API's) allow us to control services without clicking around a user interface.  In this case, we're not passing any arguments when we create the repo.  But we could.  If you want to standardize all the repositories for your organization, using the API will ensure a faster, more consistent process.

![image](create_repo_gitlab.png?)

- Take note of the `ssh_url_to_repo` field's value in the output of that command

- Clone your `mywebserver` repository locally

  ```bash
  git clone <value_of_ssh_url_to_repo>
  cd mywebserver
  ```

![image](git_clone_repo_gitlab.png?)

---

### Working With Your Repo

- Create a new file

  ```bash
  echo "hello world" > gitlablab
  ```

- Check the status

  ```bash
  git status
  ```

- Track the file

  ```bash
  git add gitlablab
  ```

- Make a commit

  ```bash
  git commit -m 'add hello world'
  ```

- Make a change

  ```bash
  echo "this is a mistake" > gitlablab
  git add gitlablab
  git commit -m 'fix the file'
  ```

- You have everything locally, but you want to push it to GitLab so...
  - You have a backup
  - You can collaborate with others  

- Store all commits on GitLab

  ```bash
  git push
  ```

- But we messed up our file.  Let's fix it.

  ```bash
  echo "Hello World" > gitlablab
  git add gitlablab
  git commit -m 'actually fixed this time'
  git push
  ```

- Just for fun, let's delete (make sure your `git push` worked!) and recover our local copy of the file...

  ```bash
  ls -l
  rm gitlablab
  ls -l
  git checkout gitlablab
  ls -l
  ```


- **[extra credit]** git diff command - enter the below commands and then git checkout prev version from last commit

  ```bash
  echo "replacement" > gitlablab
  echo "enhancement" >> gitlablab
  git diff
  ```
---

### Clone The Class Materials

```bash
cd ~/sfs
git clone https://gitlab.com/asayre/synnex-westcon-fse.git
```

---

### Create Your Own Project for This Class

```bash
cd ~/sfs
mkdir work-dir
cd work-dir
git init
```

The `git init` line tells git to start tracking this folder as a repository.  It doesn't matter that there's no remote GitLab repository.  You can still manage branches and commits locally.

---

### Further reading
  - [Gitlab API Documentation](https://docs.gitlab.com/ee/api/)
  - [Basic git terminology](http://juristr.com/blog/2013/04/git-explained/#Terminology)
  - [More advanced terminology](https://cocoadiary.wordpress.com/2016/08/17/git-terminologyglossary/)
  - [Revert a commit](https://git-scm.com/docs/git-revert.html) (a.k.a. Undo; restoring from backup)
  - [Branching and merging](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)
  - [Git best practices](http://kentnguyen.com/development/visualized-git-practices-for-team/) (There are a lot of these.  Read critically and pick what works best in your environment.)
  - [Stashing](https://git-scm.com/book/en/v1/Git-Tools-Stashing)

---

|Previous: [Bash Warmup](/labs/00_bash_warmup/README.md)|Next: [Docker](/labs/02_docker/README.md)|
|---:|:---|
