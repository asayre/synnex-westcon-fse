# Everything All At Once

*By the end of this lab, you will:*

1. attack your public facing web application (SAST vs DAST)

1. Confirm your CI/CD system is working by changing a setting on the web app firewall (WAF), committing the change, and tracking the change through the CI pipeline.

1. Confirm your web app runs through the pipeline after making a change to the website content.



  ![image](everything.png?)

---


It's time to tie everything together!  

This is what we've been working for.  Remember from the beginning of this course:
```
Faster Development + Smaller Changes = More Stability + Happier People
```

And the success metric for this class is the time it takes for a commit to be deployed.  So let's measure.  Get out your stopwatch.

---

### SAST vs DAST security testing:

*Attack your public facing web app from your local VM.*

*First be sure you have your ealstic IP captured in a variable:*

recommended working directory: `cd ~/sfs/mywebserver`

```bash
echo $EIP
```

*If it's not there, go get your Elastic IP from AWS and make a variable:*

```bash
export EIP=<your EIP here>
```


```bash
docker rm -f zap

docker run -v $(pwd):/zap/wrk/:rw --name zap -t owasp/zap2docker-weekly:latest zap-baseline.py -t "http://$EIP:80" -c generated_file.conf -r shiny_report_AWS.html

xdg-open shiny_report_AWS.html
```
---

### What happens when a CI job fails?

Open your /gitlab-ci.yml file, find the test stage, then find the 'docker run' command for the WAF, change the paranoia level of the WAF in the test stage:

```yaml

    - docker run -d -p80:8001 --name waf -e PROXY=1 -e BACKEND=http://$DGIP:8000 -e PARANOIA=1 franbuehler/modsecurity-crs-rp
```

- Commit the change

```bash
git add -A
git commit -m "PL set to 1"
git push
```

- Monitor the progress on your 'mywebserver' repo: [GitLab](https://gitlab.com)

---

### Update the Web Page

- Update the web page

recommended working directory: `cd ~/sfs/mywebserver`

  ```bash
  atom customize.yml
  ```
*make a change to the "name:" value or the "welcomeBanner: value"*
*save*

- Commit the change

  ```bash
  git add -A
  git commit -m 'web page change'
  git push
  ```

- Monitor the progress on your 'mywebserver' repo: [GitLab](https://gitlab.com)


---

### Extra Credit

- Add an environment to the `update webserver` job.

  ```yaml
  update webserver:
    stage: deploy
    environment:
      name: production
    ...
  ```

- Run a few pipelines

- Check `https://gitlab.com/<your_user_name>/mywebserver/environments`

---

### Questions for Review

- How long did it take for that commit to be deployed?
- What was the developer effort required to deploy the change?
- What was the operations effort required to deploy the change?

---

### Other Thoughts

- More complicated software will require more complicated testing, but this can all be automated in the `.gitlab-ci.yml` file.  (Which, conveniently, is stored in the code repo.)
- Ops folks might want to investigate options (scripts, [Kubernetes](https://kubernetes.io/), [Marathon](https://github.com/mesosphere/marathon), etc.) to assist with rolling updates.

---

|Previous: [Automated Deployment](/labs/09_automated_deployment/README.md)|
|:----:|
