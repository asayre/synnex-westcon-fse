# Automated Deployment

*By the end of this lab, you will:*
1. Add an `update_webserver` CI job
1. Automate the steps to deploy your webserver using a bash script
1. Add a stage to the gitlab pipeline to automate the deployment the webserver

Required working directory: `~/sfs/mywebserver`  

---

### Verify the Docker Login by Updating the Web Server

- Log in to your server.

  ```bash
  ssh ${EIP}
  ```

- On the server, you can update the webserver with the newest container by stopping/removing the current container, then pulling and starting the new one:

  ```bash
  GLORG=<your_user_name>
  sudo docker stop mywebserver
  sudo docker rm mywebserver # <----- tip: sudo docker rm -f mywebserver will stop and remove in one command

  sudo -H docker pull registry.gitlab.com/${GLORG}/mywebserver:latest
  sudo -H docker run --detach -e "NODE_ENV=customize" -p80:3000 --name mywebserver registry.gitlab.com/${GLORG}/mywebserver:latest
  ```

- But we don't want to have to log on the server every time there's a new mywebserver version.
- Log out of the server.
- Run those same commands over SSH:

  ```bash
  GLORG=<your_user_name>
  ssh ${EIP} "sudo docker stop mywebserver"
  ssh ${EIP} "sudo docker rm mywebserver"
  ssh ${EIP} "sudo -H docker pull registry.gitlab.com/${GLORG}/mywebserver:latest"
  ssh ${EIP} "sudo -H docker run -d -p80:80 --name mywebserver registry.gitlab.com/${GLORG}/mywebserver:latest"
  ```

- You can simplify that by running all the commands within a single connection:

  ```bash
  GLORG=<your_user_name>
  ssh ${EIP} "
    sudo docker stop mywebserver
    sudo docker rm mywebserver
    sudo -H docker pull registry.gitlab.com/${GLORG}/mywebserver:latest
    sudo -H docker run -d -p80:80 --name mywebserver registry.gitlab.com/${GLORG}/mywebserver:latest"
  ```

---

### Create a Deploy Job

We're going to translate the above commands into a Gitlab CI job, but with one significant change:

Instead of using `latest` version of the image, we're going to use the version specifically associated with the version of our code for a given commit (software version).

- Add a `deploy` stage to your `~/sfs/mywebserver/.gitlab-ci.yml`
- Add a deploy job:

  ```yaml
  # Our deploy job
  update webserver:
    stage: deploy
    script:
      - >
        ssh -i /tmp/my_key.pem ubuntu@${ELASTIC_IP} "
          echo Removing the mywebserver container...
          sudo docker rm -f mywebserver
          echo Updating the image...
          sudo -H docker pull ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}
          echo Starting new mywebserver container...
          sudo -H docker run --detach -e "NODE_ENV=customize" -p 8000:3000 --name mywebserver ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}
          DGIP=\$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.Gateway}}{{end}}' mywebserver)
          echo \$DGIP
          docker rm -f waf
          docker run -dti -p80:8001 --name waf -e PROXY=1 -e BACKEND=http://\$DGIP:8000 -e PARANOIA=2 franbuehler/modsecurity-crs-rp
          docker ps
        "
  ```

---

The whole thing should look something like this:

```yaml
# Tell GitLab CI to use the Docker-in-Docker image (container) to run our tasks
image: docker:stable
services:
  - name: docker:dind

# Stages will be performed in order (e.g. all fire_it_up jobs will be run at once, then all
# build_test_push jobs, then all shut_it_down jobs).
# If we tried to run the build, test, and push as separate stages, the test and push
# wouldn't have the image we built in the build stage.
stages:
  # Set up what we need to run the pipeline
  - prepare
  # Build, curl, and publish our artifact/image
  - build
  # Test webserver against OWASP threats with a WAF and OWASP ZAP attacks
  - test
  # Deploy our image/container to the EC2 server
  - deploy

before_script:
  # Grab our EC2 SSH key from our CI/CD Variables
  - echo "${SSH_KEY}" > /tmp/my_key.pem
  # Configure the permissions on our key file
  - chmod 0600 /tmp/my_key.pem
  # Clean up any previous host keys
  - ssh-keygen -R ${ELASTIC_IP} -f /etc/ssh/ssh_known_hosts || true
  # Add a new host key
  - ssh-keyscan -H ${ELASTIC_IP} >> /etc/ssh/ssh_known_hosts

docker login:
  stage: prepare
  script:
    # Create /tmp/my_key.pem with the EC2 SSH key on the Gitlab runner
    - echo "$SSH_KEY" > /tmp/my_key.pem
    - chmod 0600 /tmp/my_key.pem
    # SSH from the Gitlab runner to the webserver and log in to the Docker registry using the deploy token
    - >
      ssh -i /tmp/my_key.pem ubuntu@${ELASTIC_IP} "
      echo ${GITLAB_DEPLOY_PASS} |
      sudo -H docker login -u "${GITLAB_DEPLOY_USER}" --password-stdin registry.gitlab.com"

# This is our build job definition
build_and_curl_webserver:
  stage: build
  script:


    # Build the container much like we did in the webserver lab
    # We're tagging this image with both the build number and the 'latest' tags
    - docker build -t ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA} -t ${CI_REGISTRY_IMAGE}:latest .
    # Run the webserver container locally so we can test it
    - docker run --detach -e "NODE_ENV=customize" -p 8000:3000 --name mywebserver ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}
    # juice shop web site needs a little time to finish setting up, apparently
    - sleep 15
    # parse out the Docker Gateway IP into a variable (DGIP)
    - DGIP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.Gateway}}{{end}}' mywebserver)
    # Dump the logs so we have extra troubleshooting information in case there's an issue
    - docker logs mywebserver
    # curl test to make sure webserver is up and accessible
    - docker run --name curl curlimages/curl http://$DGIP:8000 # <---- fancy Docker way of doing a curl
    # If all the prior steps completed successfully, log in to the registry and push our image
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
    # This will result in both the build number and 'latest' tags pushing to the registry
    - docker push ${CI_REGISTRY_IMAGE}
    - echo ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}

# This is the security test
# If Owasp ZAP succeeds in compromising our webserver, the pipeline will fail
# and will not move to the deploy stage
owasp_zap_test:
  stage: test
  image: docker:stable
  services:
    - name: docker:dind

  script:
    # - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
    - docker run --detach -e "NODE_ENV=customize" -p 8000:3000 --name mywebserver ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}
    - sleep 15
    - DGIP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.Gateway}}{{end}}' mywebserver)
    - echo $DGIP
    - docker run -d -p80:8001 --name waf -e PROXY=1 -e BACKEND=http://$DGIP:8000 -e PARANOIA=2 franbuehler/modsecurity-crs-rp
    - docker run -v $(pwd):/zap/wrk/:rw --name zap -t owasp/zap2docker-weekly:latest zap-baseline.py -t "http://$DGIP:80" -c generated_file.conf -r shiny_report.html
    - docker logs zap

  artifacts: # <--------------- place a copy of the ZAP attack report in the job status page of the repo
    when: always
    paths:
      - ./*.html
    expire_in: 1 day

# Our deploy job
update webserver:
  stage: deploy
  script:
    - >
      ssh -i /tmp/my_key.pem ubuntu@${ELASTIC_IP} "
        echo Removing the mywebserver container...
        sudo docker rm -f mywebserver
        echo Updating the image...
        sudo -H docker pull ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}
        echo Starting new mywebserver container...
        sudo -H docker run --detach -e "NODE_ENV=customize" -p 8000:3000 --name mywebserver ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}
        DGIP=\$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.Gateway}}{{end}}' mywebserver)
        echo \$DGIP
        docker rm -f waf
        docker run -d -p80:8001 --name waf -e PROXY=1 -e BACKEND=http://\$DGIP:8000 -e PARANOIA=2 franbuehler/modsecurity-crs-rp
        docker ps
      "
```

---

|Previous: [Automated Authentication](/labs/08_automated_authentication/README.md)|Next: [Everything All At Once](/labs/10_everything/README.md)|
|----:|:----|
