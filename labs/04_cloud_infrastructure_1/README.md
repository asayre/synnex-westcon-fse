# Cloud Infrastructure

Part 1: Setting Up a Server in Amazon Web Services' (AWS) Elastic Compute Cloud (EC2)  

*By the end of this lab, you will:*

- Describe what 'the cloud' is
- Describe a Region vs. an Availability Zone
- Deploy a compute instance in AWS
- Use a Cloud Formation Template (CFT)


![image](aws-regions.png?)


Recommended working directory: `~/sfs/work-dir/cloud_infrastructure`

---
### Create Directory

```bash
- cd ~/sfs/work-dir
- mkdir cloud_infrastructure
- cd cloud_infrastructure
```

---

### Verify Your AWS Access

- Log in to the [AWS console](https://console.aws.amazon.com/) at [https://console.aws.amazon.com/](https://console.aws.amazon.com/)
- Navigate to EC2 (under the Services menu at the top of the console)
- Make sure you are operating in the N. Virginia (us-east-1) or Oregon (us-west-2) region.
- Suggestion: Pin the EC2 and CloudFormation shortcuts to your toolbar.

---

### Create an SSH Key

- From the left side of the EC2 page, select "Key Pairs"
- Click the "Create Key Pair" button
- Give the key pair a name (e.g. my_key), use pem format
- If prompted, save the private key to ~/.ssh/
- If not prompted, you may have to move the key

  ```bash
  mv ~/Downloads/my_key.pem ~/.ssh/my_key.pem
  ```

- Either way, you'll need to set the permissions on the private key file:

  ```bash
  chmod 0600 ~/.ssh/my_key.pem
  ```

**Note**: The filename of your copy of the key does not have to match the name of the key in the cloud.  For example, you could create a `OneRing-2019-10-19` key in EC2 (which would download a `OneRing-2019-10-19.pem` file), then rename the local file to `~/.ssh/RulesThemAll.pem`.  If you do this, you'll have to be cognizant of which name to use in which places.  For the context of our labs, `my_key.pem` (usually prepended with `~/.ssh/`) will always be the local name, whereas `my_key` (no `.pem`) will be the AWS/CloudFormation/EC2 name.

---

### Allocate an Elastic IP

We need this so our IP won't change for the duration of the labs
- Go to the AWS EC2 console and click `Elastic IPs` on the left-side menu
- Click `Allocate New Address`
- Take note of the **Allocation ID**

---

### Define the Infrastructure

- Find a [demo_stack.yml](files/demo_stack.yml) file in 04_cloud_infrastructure_1/files
  - Save this file at `~/sfs/work-dir/cloud_infrastructure/demo_stack.yml`

  ```bash
  cd ~/sfs/work-dir/cloud_infrastructure/
  cp ~/sfs/synnex-westcon-fse/labs/04_cloud_infrastructure_1/files/demo_stack.yml .
  ```

**AWS resources used/created by this cloud formation template (demo_stack.yml):**

 - **[AMI]:** Golden VM image used to create running EC2 instances

 - **[EC2]:** AWS version of a VM

 - **[Elastic IP (EIP) and allocation ID]:** Public facing IP address that you create and attach it to any EC2 resource that you create. Each EIP has a **corresponding allocation ID** associated with it.

 - **[Security Group]:** AWS way of creating security policies (port 80 and 22 = open, close the rest!)

 - **[SSH key]:** Authentication cert key (my_key.pem)

 - **[VPC]:** Virtual Private Cloud - logical sets of resources you create in AWS
(EC2, EIP, SecGroup, etc....)

 - **[other structures]:** InternetGateway, MyRouteTable, MySubnet


---


Open the file demo_stack.yml:

```bash
cd ~/sfs/work-dir/cloud_infrastructure/demo_stack.yml
atom demo_stack.yml
```


  - In this file update `Resources.MyServer.Properties.KeyName` to match the key you created above

    ```yaml
    Resources:
      ...
      MyServer:
        ...
        Properties:
          ...
          ##### UPDATE THIS KEY NAME #####
          KeyName: update_this_value
          ...
    ```

  - Now update the `Resources.MyElasticIP.AllocationID` to match the one you created above

    ```yaml
    Resources:
      ...
      MyElasticIP:
        ...
        ##### UPDATE THIS ALLOCATION ID #####
        AllocationId: update_this_value
        ...
    ```

  - Update the words after "echo" in the `UserData` Section

    ```yaml
    Resources:
      ...
      MyServer:
        ...
        Properties:
          ...
          UserData:
            "Fn::Base64":
              "Fn::Join":
                - "\n"
                - - "#!/bin/bash"
                  # Launch scripts go here...
                  - echo Hello World!
    ```

---


### Build the Infrastructure

- From the [CloudFormation console](https://console.aws.amazon.com/cloudformation/)
- Click the "Create Stack" button
- Select the "Upload a template file" option
- Upload the `demo_stack.yml` file you've modified
- Give the stack a name
- Click through the rest of the GUI, picking the defaults

![image](aws.png?)

---

### Check Your Server

- From the [EC2 console](https://console.aws.amazon.com/ec2/), find the public IP of your instance
  - This should be the elastic IP that you created above
  - Store your Elastic IP as an environment variable:

    ```bash
    export EIP=<elastic_IP>
    ```

- Log in to the server

  ```bash
  ssh -i ~/.ssh/my_key.pem ubuntu@${EIP}
  ```

- Are you in?  If so, you made a server!
- Check the build logs, then see if Docker is installed

  ```bash
  cat /var/log/cloud-init-output.log
  # Look for your echo command!
  docker --version
  ```

---

### Things to Note
- This server is not very secure (SSH open to the world)
- No automated resiliency or redundancy
- We didn't install any configuration management

---

|Previous: [WebServer](/labs/03_webserver/README.md)|Next: [Cloud Infrastructure 2](/labs/05_cloud_infrastructure_2/README.md)|
|----:|:----|
