# Automated Authentication

*By the end of this lab, you will:*
1. Authenticate from your server to Gitlab
1. Authenticate from Gitlab to your server

Required working directory: `~/sfs/mywebserver`  

---

## Configure Authentication

##### **Deploy Token** for server-to-Gitlab authentication

- Create a **deploy token** in your Gitlab project
  - Navigate to your `MyWebserver` project
  - `Settings` --> `CI/CD` --> Deploy Tokens
  - Choose a name like `mywebserver-20191019`
  - Check the `read_registry` box; leave everything else blank
  - Click the `Create deploy token` button
  - Take note of the username and password on the next page.  Copy these somewhere safe temporarily so you don't lose them.  Gitlab won't show them to you again
    - Example username: `gitlab+deploy-token-102968`
    - Example password: `SV69GwbWLFu7pLnW3CE4`
  - Now navigate to `Settings` --> `CI/CD` --> `Variables`
  - Create two variables:
    - `GITLAB_DEPLOY_USER`: `<your-deploy-token-username>`
    - `GITLAB_DEPLOY_PASS`: `<your-deploy-token-password>`
  - Toggle the `Protected` and `Masked` settings for the password variable
  - Save these variables

##### **SSH Key** for Gitlab-to-server authentication

- Navigate to `Settings` --> `CI/CD` --> `Variables` again
- Add a variable called SSH_KEY
- Copy and paste the private key in as the variable value

  ```bash
  # To get the contents of your private key:
  cat ~/.ssh/my_key.pem
  ```

- Make sure to copy the entire multi-line key.  The `BEGIN` and `END` lines, too.  
- Toggle the `Protected` setting for the SSH_KEY variable
- Save the variables

##### Tell Gitlab where the server is...

- Add another variable in Gitlab
- Find the IP of your webserver
- Add the IP as a CI/CD variable called `ELASTIC_IP`
- Save the variables

##### With all the variables, it should look something like this:

![image](cicd_vars.png?)

**Note**: If you manage an organization in Gitlab, you can manage these variables at group- (or subgroup-) level to enable role-based authentication and easier secret rotation.

---

### Authenticate Gitlab-to-Server in CI

- A global `before_script` will run before every job in your pipeline.
- Add this one to `~/sfs/mywebserver/.gitlab-ci.yml`:

  ```yaml
  before_script:
    # Grab our EC2 SSH key from our CI/CD Variables
    - echo "${SSH_KEY}" > /tmp/my_key.pem
    # Configure the permissions on our key file
    - chmod 0600 /tmp/my_key.pem
    # Clean up any previous host keys
    - ssh-keygen -R ${ELASTIC_IP} -f /etc/ssh/ssh_known_hosts || true
    # Add a new host key
    - ssh-keyscan -H ${ELASTIC_IP} >> /etc/ssh/ssh_known_hosts
  ```

  **Note**: We are purging and re-adding the key because we don't have the ability to input "yes" on our initial connection.  Another option would be to use the `-o StrictHostKeyChecking=no` option on all of our SSH commands.

---

### Authenticate Server-to-Gitlab (Registry) in CI

- Add a `prepare` stage to your `.gitlab-ci.yml`
- Add a CI job to install/update the credentials on the server

  ```yaml
  docker login:
    stage: prepare
    script:
      # This syntax (YAML folded-style block) replaces newlines with spaces
      # We're SSH'ing to the server to login to the Docker registry as root
      - >
        ssh -i /tmp/my_key.pem ubuntu@${ELASTIC_IP} "
          echo ${GITLAB_DEPLOY_PASS} |
          sudo -H docker login -u "${GITLAB_DEPLOY_USER}" --password-stdin registry.gitlab.com
        "
  ```

The whole thing should look something like this:

```yaml
image: docker:stable
services:
  - name: docker:dind

stages:
  - prepare
  - build
  - test

before_script:
  # Grab our EC2 SSH key from our CI/CD Variables
  - echo "${SSH_KEY}" > /tmp/my_key.pem
  # Configure the permissions on our key file
  - chmod 0600 /tmp/my_key.pem
  # Clean up any previous host keys
  - ssh-keygen -R ${ELASTIC_IP} -f /etc/ssh/ssh_known_hosts || true
  # Add a new host key
  - ssh-keyscan -H ${ELASTIC_IP} >> /etc/ssh/ssh_known_hosts

docker login:
  stage: prepare
  script:
    # This syntax (YAML folded-style block) replaces newlines with spaces
    # We're SSH'ing to the server to login to the GitLab Docker registry as root
    - >
      ssh -i /tmp/my_key.pem ubuntu@${ELASTIC_IP} "
        echo ${GITLAB_DEPLOY_PASS} |
        sudo -H docker login -u "${GITLAB_DEPLOY_USER}" --password-stdin registry.gitlab.com
      "

build_owasp-juice_shop:
  stage: build
  script:
    - docker build -t ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA} -t ${CI_REGISTRY_IMAGE}:latest .
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
    - docker push ${CI_REGISTRY_IMAGE}
    - docker run --detach -e "NODE_ENV=customize" -p 8000:3000 --name mywebserver ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}
    - docker logs mywebserver
    - DGIP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.Gateway}}{{end}}' mywebserver)
    - sleep 15
    - docker run --name curl curlimages/curl http://$DGIP:8000 # <---- fancy Docker way of doing a curl
    - docker push ${CI_REGISTRY_IMAGE}
    - echo ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA} published.

# This is the security test
# If Owasp ZAP succeeds in compromising our webserver, the pipeline will fail
# and will not move to the deploy stage
owasp_zap_test:
  stage: test
  image: docker:stable
  services:
    - name: docker:dind

  script:
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
    - docker run --detach -e "NODE_ENV=customize" -p 8000:3000 --name mywebserver ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}
    - sleep 15
    - DGIP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.Gateway}}{{end}}' mywebserver)
    - echo $DGIP
    - docker run -d -p80:8001 --name waf -e PROXY=1 -e BACKEND=http://$DGIP:8000 -e PARANOIA=2 franbuehler/modsecurity-crs-rp
    - docker run -v $(pwd):/zap/wrk/:rw --name zap -t owasp/zap2docker-weekly:latest zap-baseline.py -t "http://$DGIP:80" -c generated_file.conf -r shiny_report.html
    - docker logs zap

  artifacts: # <--------------- place a copy of the ZAP attack report in the job status page of the repo
    when: always
    paths:
      - ./*.html
    expire_in: 1 day

```

- Commit this change to your `.gitlab-ci.yml` and make sure the pipeline runs.
- You should see `WARNING! Your password will be stored unencrypted in /root/.docker/config.json.` in the output of the `docker login` job.  This means that anyone with root access to the server will be able to get access to the Docker registry.  But since this is a deploy token for this single project/repository, the scope of access is limited.

---

|Previous: [Manual Deployment](/labs/07_manual_deployment/README.md)|Next: [Automated Deployment](/labs/09_automated_deployment/README.md)|
|----:|:----|
