# A WebServer, WAF, and ZAP (in Docker)

*By the end of this lab, you will be able to:*

 * build and run a local webserver container
 * select container images purposefully for your docker builds



For this lab, use the repository you created in the [GitLab lab](../01/gitlab.md).  
If you followed the instructions verbatim, it should be `~/sfs/mywebserver`

```bash
cd ~/sfs/mywebserver
```

copy a couple of file from the class material into your working directory (we'll use them a bit later in this lab):

```bash
cp ~/sfs/synnex-westcon-fse/labs/03_webserver/files/customize.yml .
cp ~/sfs/synnex-westcon-fse/labs/03_webserver/files/generated_file.conf .
```

verify they copied over:

```bash
ls
```

---

### Make a Webserver with Docker

Save this as a `Dockerfile` file:

```yml
FROM bkimminich/juice-shop
COPY customize.yml ./config

```

And build your Docker image:

```bash
docker build -t webserver .
```

![image](make_webserver_juiceshop.png?)


---


### Run and Test Your Webserver

Run your webserver with the `-p 8000:3000` option to expose the ports, and give it a name with the `--name` flag:

```bash
docker run --detach -e "NODE_ENV=customize" -p 8000:3000 --name mywebserver webserver
```

Now run `docker ps` to see information about your running container:

```
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
6e1b4f9995ab        webserver           "docker-entrypoint.s…"   12 minutes ago      Up 12 minutes       0.0.0.0:8000->3000/tcp   mywebserver
```

Finally, test your webserver to see if it's running:

```bash
xdg-open http://localhost:8000

```

You should also be able to test http://localhost:8000 in your web browser.

---

### WAF and OWASP ZAP


[what we'll build]:


![image](container_setup.png?)


*first inspect your webserver container for the Docker Gateway IP address, store IP address in a variable called DGIP:*
```bash
DGIP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.Gateway}}{{end}}' mywebserver)
```
*verify the Docker Gateway IP address (DGIP) was captured in the variable:*

```bash
echo $DGIP
```

*attack your webserver directly (no WAF set up yet, attack port 8000), generate a report of the results:*

```bash
docker run -v $(pwd):/zap/wrk/:rw --name zap -t owasp/zap2docker-weekly:latest zap-baseline.py -t "http://$DGIP:8000" -c generated_file.conf -r shiny_report_no_waf.html
```

*review the vulnerability report:*

```bash
xdg-open shiny_report_no_waf.html
```

*clearly a lot of security gaps*

*now put a WAF in front of your webserver (set PARANOIA level to 2):*

```bash
docker run -d -p80:8001 --name waf -e PROXY=1 -e BACKEND=http://$DGIP:8000 -e PARANOIA=2 franbuehler/modsecurity-crs-rp
```

*remove the ZAP docker image you just ran:*

```bash
docker rm -f zap
```

*run ZAP again but now attack your webserver INDIRECTLY (through the WAF, port 80), change report name:*

```bash
docker run -v $(pwd):/zap/wrk/:rw --name zap -t owasp/zap2docker-weekly:latest zap-baseline.py -t "http://$DGIP:80" -c generated_file.conf -r shiny_report_waf_p2.html
```
*review the vulnerability report:*

```bash
xdg-open shiny_report_waf_p2.html
```

*number of successful attacks are reduced to almost none!*

# Docker Hub - hub.docker.com
  - [Explore](https://hub.docker.com/search?&q=)

|Previous: [Docker](/labs/02_docker/README.md)|Next: [Cloud Infrastructure 1](/labs/04_cloud_infrastructure_1/README.md)|
|----:|:----|
