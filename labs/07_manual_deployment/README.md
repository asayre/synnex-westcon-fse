# Manual Deployment

*By the end of this lab, you will:*

1. Run the webserver on your EC2 instance

Recommended working directory: `~/sfs/work-dir/cloud_infrastructure`

---

### Authenticate to the Registry

- To pull the image from the server, use your Gitlab username and password to log in to Docker *from the server*:

  ```bash
  ssh ${EIP}
  docker login registry.gitlab.com
  ```

- **NOTE**: If you have two-factor authentication enabled, you need to use a personal access token (with the "api" scope) as your password.  Your actual password won't work.

---

### Run the Webserver on the Server

- Go to the AWS EC2 console and find the public IP of your instance
- Log in to the server and try to run your container, using the container you pushed to GitLab

  ```bash
  ssh ${EIP}
  docker run --detach -e "NODE_ENV=customize" -p 80:3000 --name mywebserver registry.gitlab.com/<gitlab-username>/mywebserver:latest
  sudo docker logs mywebserver
  ```

- If everything looks good, see if you can access your webpage
- While on the server:

  ```bash
  curl localhost:80
  ```

![image](manual_deploy.png?)

- Exit the EC2 instance. From your VM try to access the server's public IP in a web browser (and/or using a curl command)

  ```bash
  xdg-open "http://${EIP}"
  curl ${EIP}
  ```

---

|Previous: [Continuous Integration](/labs/06_continuous_integration/README.md)|Next: [Automated Authentication](/labs/08_automated_authentication/README.md)|
|----:|:----|
