# Cloud Infrastructure

Part 2: Tear It Down! (And Iterate!)

*By the end of this lab you will be able to:*

- Reuse and change a Cloud Formation Template
- Describe a change in AWS infrastructure vs a change in an EC2 instance
- Relish in the beauty of auto-deployed infrastructure

Recommended working directory: `~/sfs/work-dir/cloud_infrastructure`

---

One of the fundamental concepts in the agile methodology is an [iterative approach](https://en.wikipedia.org/wiki/Agile_software_development#Iterative_vs._Waterfall).  Avoiding [snowflakes](http://martinfowler.com/bliki/SnowflakeServer.html) is a critical aspect of the speed and agility we're trying to attain with DevOps.  Losing a server should never induce a panic.

---

### Tear Down Your Infrastructure
- Navigate to the CloudFormation console
- Select the stack you previously created
- Click the "Actions" button and select "Delete Stack"

---

### Make an Improvement
- Build out the UserData script in the `demo_stack.yml`
  - Update the server when it first boots
  - Install Docker
  - Add the ubuntu user to docker group
  - Reboot (in case any updates require it)

  ```yaml
  UserData:
    "Fn::Base64":
      "Fn::Join":
        - "\n"
        - - "#!/bin/bash"
          - apt update
          - apt install -y docker.io
          - usermod -aG docker ubuntu
          - reboot
  ```

  Double-check your indentation!

---

### Rebuild the Stack
- Use CloudFormation to build your newly updated stack!

![image](aws2.png?)

---

### Check your changes
- Go to the AWS EC2 console and find the public IP of your instance
  - This should be the elastic IP that you created in the previous lab
- Log in to the server and verify the Docker install

  ```bash
  ssh -i ~/.ssh/my_key.pem ubuntu@${EIP}
  docker --version
  ```

You will likely see "WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!"  This is because the "fingerprint" of your cloud server has changed. (It's a totally new/different server, right?)  SSH is trying to protect you.  

Here's what we're going to do for this class.  Other options are listed below.  Add this block to your `~/.ssh/config` file:

```bash
# Create defaults for this single Elastic IP
Host <elastic_ip>

    # Always default to the 'ubuntu' user
    User ubuntu

    # Always default to my_key.pem
    IdentityFile ~/.ssh/my_key.pem

    # Ignore changes in the EC2 host identity
    StrictHostKeyChecking no
    UserKnownHostsFile /dev/null
```

Make sure to substitute your actual Elastic IP into that `Host` line.

Don't forget to SSH into your server again.  With these defaults, you can now SSH to your server with this simple command:

```bash
ssh ${EIP}
```

The defaults for <elastic_ip> in `~/.ssh/config` make the above command equivalent to this one:

```bash
ssh -i ~/.ssh/my_key.pem -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ubuntu@${EIP}
```

---

#### Not Class Materials:

Some other options for dealing with changing host keys:

1. **Good** - Update the key.  Do this when you _know_ the host has changed behind the IP.

  ```bash
  # Remove the old host key(s) from ~/.ssh/known_hosts
  ssh-keygen -R ${EIP}

  # Add the new key to known_hosts (disables the yes/no prompting for your next SSH)
  ssh-keyscan -H ${EIP} >> ~/.ssh/known_hosts
  ```

2. **Okay** - Disable the HostKeyChecking for a single connection:

  ```bash
  ssh -i ~/.ssh/my_key.pem -o StrictHostKeyChecking=no ubuntu@${EIP}
  ```

3. **Meh?** - Disable HostKeyChecking completely:

  This has potentially serious security ramifications.  If you don't know what this means, don't do it.  But it can be extremely useful if you're managing high volumes of changing infrastructure.

  ```bash
  echo "StrictHostKeyChecking no\nUserKnownHostsFile /dev/null\n$(cat ~/.ssh/config) > ~/.ssh/config
  ```

---

### Things to Note
- It's really easy to tear it all down, make changes, and build it up again.

---

### Relevant Topics to Research on Your Own
- Configuration Management
- Immutable Infrastructure
- Agile Methodology

---

|Previous: [Cloud Infrastructure 1](/labs/04_cloud_infrastructure_1/README.md)|Next: [Continuous Integration](/labs/06_continuous_integration/README.md)|
|----:|:----|
