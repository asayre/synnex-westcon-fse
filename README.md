# Intro to DevOps

**This class will:**

- Build, connect, and examine the basic elements of a CI/CD pipeline
- Create an application, and walk it through a simple development life-cycle (prepare, build, test, deploy)
- Demonstrate steps to add security to a CI/CD pipeline, without disrupting the SDLC
- Demonstrate the value of a SW Defined Web Application Firewall (waf)

---

### Original Author's Notes:

DevOps is an exceptionally broad discipline.  It requires development expertise, operations sensibility, an immense capacity for troubleshooting, eagerness to learn, and - maybe most importantly - a broad base of knowledge around tools that can be stitched together to accomplish the goal.  

This class will provide you with a very basic framework for workflow automation.  Every commit to a repo will be built, tested, and deployed.

There is no Holy Grail - every environment is different.  This course attempts to use mostly free, open-source software to show that workflow automation is a real thing that can be done.  We'll cover one way to do it, but there are many, many other ways to accomplish the same thing.  (I've also done a GitHub --> CircleCI --> AWS/Mesos/Marathon setup that does something very similar to what we'll see in this course.)

- [Aaron Brown](https://www.linkedin.com/in/aayore/), Sr. Software Team Manager, Verizon

---


Teachers:

- [Anthony Sayre](https://www.linkedin.com/in/anthony-sayre-1a92263a/), Field Sales Engineer, Synnex Westcon
- [Silvia Briscoe](https://www.linkedin.com/in/silviawessels), Systems Engineer III, F5 - Verizon

### Success Metric

Measure - or estimate - the time it takes for a code commit to be deployed in your production environment.  This is the main metric we'll use to measure the success of the DevOps workflow we build in this class.

---

### Prerequisites:

- Before starting this class, you should have a basic familiarity with a Unix-like shell (sh, bash, zsh, ksh, etc.)
- Laptop
  - Something with a Bash-like shell is preferred (Mac or Linux)
  - Windows machines may be a challenge (Maybe use [VirtualBox](https://www.virtualbox.org/) to run a Linux VM?)
  - Make sure you have [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) installed
- [Create a GitLab account](https://gitlab.com/users/sign_in)
  - If you already have one, you can disregard this
  - If you create a new one, you might consider making it something that you could take from company to company
- [Install Docker on your laptop](https://hub.docker.com/search/?type=edition&offering=community)
- [Create a free-tier AWS account](https://aws.amazon.com/)
- Mac Users - We recommend [Atom](https://atom.io/) **unless you are familiar with VI

***

### Following Instructions

Please follow the documented and stated instructions as closely as possible.  This will help mitigate issues that arise due to funky configurations.  As mentioned above, we'll be stitching together a number of tools.  This means that the labs are very inter-dependent, and an innocent deviation in an early lab could complicate a later lab.

I encourage all students to experiment and explore the material.  Making it your own and having fun with it will probably increase the functional utility of this class immensely.  I'm happy to help with any extracurricular questions and/or interests related to the material, but please try the extra stuff after completing the suggested stuff.  And maybe in a different subdirectory.  :)

***



[Visual of what we'll build]:

![image](basic-gitlab-ci.png?)

---

[SW Developer's View]:

![image](devops-activities.png?)

---

[Security View]:

![image](devsecops-activities.png?)

---

# Labs

### _Fundamentals_

### 0. [Bash Warmup](labs/00_bash_warmup/README.md)

### 1. [GitLab](labs/01_gitlab/README.md)

### 2. [Docker](labs/02_docker/README.md)

### 3. [Webserver, WAF, and OWASP ZAP](labs/03_webserver/README.md)

### 4. [Cloud Infrastructure 1](labs/04_cloud_infrastructure_1/README.md)

### 5. [Cloud Infrastructure 2](labs/05_cloud_infrastructure_2/README.md)

### 6. [Continuous Integration](labs/06_continuous_integration/README.md)

### 7. [Manual Deployment](labs/07_manual_deployment/README.md)

### 8. [Automated Authentication](labs/08_automated_authentication/README.md)

### 9. [Automated Deployment](labs/09_automated_deployment/README.md)

### 10. [Everything All At Once](labs/10_everything/README.md) (a.k.a. The Easy Lab)

***

### Bonus Labs

### + [Configuration Management](/labs/bonus/configuration_management.md)

### + [Team Organization](labs/bonus/team_organization.md)

---

### Recommended Reading
- [The Phoenix Project](https://www.amazon.com/Phoenix-Project-DevOps-Helping-Business/dp/0988262509/), by Gene Kim
  - This is a rewrite of *The Goal* for the modern age.
- [The Goal](https://www.amazon.com/Goal-Process-Ongoing-Improvement/dp/0884271951/), by Eliyahu Goldratt
  - This is the original, and I think you'll benefit from doing the modern adaptation yourself.

---

### Concessions: Things This Course Does Poorly
- Don't use the [root account](http://docs.aws.amazon.com/general/latest/gr/root-vs-iam.html) with AWS.  Create your own IAM user.
- Use [public and private subnets](http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Scenario2.html) with your VPC.  Unless you're doing the zero-trust thing.
- Set up a [VPN](http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/vpn-connections.html) to your VPC.  Unless you're doing the zero-trust thing.



### links to other mats used in the course:

- https://www.owasp.org/images/0/0f/Devsecops-owasp-indonesia.pdf
